package groovyedit

import groovy.beans.Bindable

class FilePanelModel {
   // @Bindable String propName
   
   File loadedFile
   @Bindable String fileText
   @Bindable boolean dirty
   String mvcId
}