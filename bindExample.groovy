import groovy.beans.Bindable
import groovy.swing.SwingBuilder
swing = new SwingBuilder()

class BindSample {
	@Bindable String foo
	String bar
}
sample = new BindSample(foo: 'One', bar: 'Two')
swing.bind(source: sample, sourceProperty: 'foo',
target: sample, targetProperty: 'bar')

println "foo=$sample.foo bar=$sample.bar"
sample.foo = 'Three'
println "foo=$sample.foo bar=$sample.bar"
